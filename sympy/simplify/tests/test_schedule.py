from sympy.core import symbols, Integer, Symbol, Tuple, oo
from sympy import (Add, Mul, Pow, Symbol, exp, sqrt, symbols, sympify, cse,
    Matrix, S, cos, sin, Eq, schedule)

from sympy.utilities.pytest import raises

# Import test:
from sympy.simplify.schedule import gen0, digraph_from_cse, schedule_digraph, pebble_digraph

# Can't test this module if nx is not present:
from sympy.simplify.schedule import nx

sym_of_num = lambda i: Symbol("y.%d"%i)
l1 = [sym_of_num(i) for i in range(0,4)]
l2 = [sym_of_num(i) for i in range(4,8)]
def unary(f, l):
	return [f(x) for x in l]
def byelem(f, l):
	return [f(*x) for x in l]

e1 = unary(lambda x: Mul(x, -1), byelem(Add, zip(l1,l2)))
e2 = byelem(Add, zip(l1, reversed(l2)))
e3 = byelem(Add, zip(e1, e2))
e4 = byelem(Mul, zip(e1, e2, e3))

def test_schedule():
	steps, out = cse(e4)
	g = gen0("z")
	sch, inp = schedule([(g.next(), e) for e in e4])
	assert len(sch) == len(steps)+len(out)
	#assert len(inp) == 

def test_digraph_from_cse():
	if not nx:
		return
	
	gz = gen0("z")
	inp = l1+l2
	sub = [(gz.next(),e) for e in e2]
	out = [(gz.next(),s[1]+t) for s,t in zip(sub,l2)]
	node, G, inp_n, out_n = digraph_from_cse(inp, sub, out)
	
	assert all([node[i][0] == j for i,j in zip(inp_n,inp)])
	assert all([node[i][0] == j[0] for i,j in zip(out_n,out)])
	assert len(node) == len(inp) + len(sub) + len(out)

def test_schedule_digraph():
	if not nx:
		return
	
	G = nx.DiGraph()
	G.add_edges_from([(1,3), (2,4), (3,5), (4,5), (2,6)])
	sch = schedule_digraph(G, [1,2], [5,6])
	assert sch[:4] == [1,3,2,4] or sch[:4] == [2,4,1,3]
	assert set(sch[4:6]) == set([5,6])

def test_pebble_digraph():
	if not nx:
		return
	
	G = nx.DiGraph()
	G.add_edges_from([(1,3), (2,4), (3,5), (4,5), (2,6)])
	red, blue = pebble_digraph(G, [1,2], [5,6])
	assert set(red) == set([1,2,3,4])
	assert set(blue) == set([5,6])

