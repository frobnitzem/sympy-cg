# Module to produce a DAG intermediate representation of the
# expression tree and determine an appropriate execution order.

from cse_main import cse
from sympy.core import Symbol
from sympy.core.compatibility import is_sequence
from sympy.external import import_module

nx = import_module('networkx')#, min_python_version=(2, 6), warn_old_version=False)

# Creates an evaluation order for common sub-expressions
# input:  f        -- List of (name, sympy expression) pairs forming the
#                     eventual output of the expression sequence.
# output: sch_op   -- List of (name, expression) pairs that can
#                     be executed in order with (hopefully) good caching behavior.
#         inp      -- List of source symbols required as inputs
def schedule(f):
	if not is_sequence(f[0]):
		f = [f]
	
	# Grok the set of input variables.
	inp = reduce(lambda x,y: x | y[1].free_symbols, f, set())
	
	sub, out = cse([p[1] for p in f])
	out = [(p[0],e) for p,e in zip(f,out)] # re-name the output nodes.
	
	if nx == None: # Can't apply the algo., just return what we have.
		return sub+out, inp
	
	node, G, inp_nodes, out_nodes = digraph_from_cse(inp, sub, out)
	sch = schedule_digraph(G, inp_nodes, out_nodes) # list of node numbers
	# the membership test here eliminates the initial copy-in operations
	sch_op = [node[i] for i in sch if i not in inp_nodes]
	
	return sch_op, inp

# Creates a DiGraph object representing the overall computation.
# input:  inp  -- a set of input variables
#         sub  -- a list of sub-expressions
#         out  -- a list of terminal sub-expressions
# output: node -- a dictionary from node numbers to (var name, sympy expression)
#         G    -- a digraph object, where each node represents a computation expression
#                 and each edge represents an intermediate (or i/o) variable.
#         inp  -- node numbers comprising input set
#         out  -- node numbers comprising output set
#
# The generated graph contains a "copy-in" node for each input variable.
# They're easy to spot, since the numbers of those nodes are returned as "inp"
def digraph_from_cse(inp, sub, out):
	node = {} # mapping from nodes to sympy expressions
	rnode = {} # reverse mapping from vars to nodes
	G = nx.DiGraph()
	
	# Visit all nodes in the following order, and execute
	# f on (node num, result name, computation expr)
	def visit(f):
	    for i,x in enumerate(inp): # copy operation
		f(i, x, x)	# We have names, so order is unimportant.
	    i = len(inp)
	    for j,(x,e) in enumerate(sub): 
		f(i+j,x,e) 
	    i += len(sub)
	    for j,(x,e) in enumerate(out):
		f(i+j,x,e)
	
	# first pass -- declare all nodes
	def declare_node(i, x, f):
		node[i] = (x,f)
		rnode[x] = i
	visit(declare_node)
	
	# second pass -- add all connections
	def connect_node(i, x, f):
		if i < len(inp): return # skip source nodes
		pred = [rnode[y] for y in f.free_symbols]
		G.add_edges_from([(j,i) for j in pred])
	visit(connect_node)
	
	return node, G, range(len(inp)), range(len(node)-len(out), len(node))

def iter_map(f, g):
	for x in g:
		yield f(x)

def any_intersect(s1, s2):
	return any(iter_map(lambda x: x in s2, s1))

# Creates a linear schedule for visiting the nodes in a digraph
# according to the red-blue pebbling game, where all
# input nodes are colored red, and all output nodes colored blue,
# followed by alternate red and blue coloring phases.
# The schedule returned visits all the red nodes before visiting
# any of the blue nodes.
def schedule_digraph(G, inp, out):
	uG = G.to_undirected()
	sch = []
	inp = set(inp)
	out = set(out)
	for sg_nodes in nx.connected_components(uG):
		snode = set(sg_nodes)
		sinp = snode & inp
		sout = snode & out
		sG = G.subgraph(sg_nodes)
		red, blue = pebble_digraph(sG, sinp, sout)
		if len(red) > len(sinp):
		      red_out = [n for n in snode \
				if any_intersect(sG.successors_iter(n), blue)]
		      sch += schedule_digraph(sG.subgraph(red), sinp, red_out)
		else:
		      sch += red
		if len(blue) > len(sout):
		      blue_inp = [n for n in snode \
				if any_intersect(sG.predecessors_iter(n), red)]
		      sch += schedule_digraph(sG.subgraph(blue), blue_inp, sout)
		else:
		      sch += blue
	return sch
	
# This routine assumes that the graph is connected.
# If this is not the case, some of the nodes will still be marked black at the end
# of the algorithm.
# 1 = red
# 2 = blue
# 3 = black
# 4 = grey -- marked to be visited
def pebble_digraph(G, inp, out):
	mark = dict((i,3) for i in G.nodes_iter()) # mark set
	visit = [] # bfs queue
	red = []
	blue = []
	
	# Mark and add next steps to visit list.
	def mark_red(i):
		mark[i] = 1
		red.append(i)
		for j in G.successors(i):
		    if mark[j] == 3:
			mark[j] = 4
			visit.append(j)
	def mark_blue(i):
		mark[i] = 2
		blue.append(i)
		for j in G.predecessors(i):
		    if mark[j] == 3:
			mark[j] = 4
			visit.append(j)
	
	# Initial pass.
	for i in inp:
		mark_red(i)
	
	nb = len(visit)
	for i in out:
		try:
			visit.pop(visit.index(i)) # Hijacked to blue!
			nb -= 1
		except ValueError:
			pass
		try:
			red.pop(red.index(i)) # Hijacked to blue!
		except ValueError:
			pass
		mark_blue(i)
	
	# Iterative phase:
	while visit:
	    nr = len(visit)
	    for k in range(nb-1,-1,-1): # don't check blue's added visits this round.
		i = visit[k]
		if all(iter_map(lambda x: mark[x] == 1, G.predecessors_iter(i))):
			visit.pop(k)
			nr -= 1
			mark_red(i)
	    nb = len(visit)
	    for k in range(nr-1,-1,-1): # don't check red's added visits this round.
		i = visit[k]
		if all(iter_map(lambda x: mark[x] == 2, G.successors_iter(i))):
			visit.pop(k)
			nb -= 1
			mark_blue(i)
		
	return red, blue

def gen0(base=".default"):
	i = 0
	while True:
		yield Symbol("%s.%d"%(base,i))
		i += 1

